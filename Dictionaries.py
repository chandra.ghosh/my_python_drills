#Write a function to find the number of occurrences of each word in a string(Don't consider punctuation characters)

def occurences(str):
    str=str.split(" ")
    unique_str=[]
    for i in str:
        if i not in unique_str:
            unique_str.append(i)
    for i in range(0, len(unique_str)):
        print(unique_str[i] + ":", str.count(unique_str[i]))

#Define a function that prints all the items (keys, values) in a dictionary
def keys_values(dict):
        for k,v in dict.items():
                print(k+" ",v)
        

#Define a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.
def sorted_list(dict):
       str=[]
       for v in dict.keys():
                str.append((v, dict[v]))
       str.sort()
       print(str)


occurences("Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported.")
keys_values({'c':2, 'b':3, 'a':1})
sorted_list({'c':2, 'b':3, 'a':1})
