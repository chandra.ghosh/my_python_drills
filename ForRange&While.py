x = [1, 2, 3, 4]
#Print all the elements in x
for i in range(len(x)):
	print(x[i])

#Print all the elements and their indexes using the enumerate function
for e in enumerate(x):
	print e

#Print all the integers from 10 to 0 in descending order using range
for i in reversed(range(11)):
	print i 

for j in range(10, -1, -1):
	print j

#Print all the integers from 10 to 0 in descending order using while
k=10
print("printing using while")
while k>=0:
	print k
	k=k-1
	print("For_Range_While")