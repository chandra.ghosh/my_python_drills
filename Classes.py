#Create a class called Point which has two instance variables, x and y that represent the x and y co-ordinates respectively. 
#Initialize these instance variables in the __init__ method
import math
class Point:
    x=0
    y=0
    def __init__(self):
        self.x=3
        self.y=4
#Define a method, distance on Point which accepts another Point object as an argument and returns the distance between the two points.
    def distance(self, x1,y1):
        return ((x1-self.x)**2+(y1-self.y)**2)**0.5

p=Point()
print(p.distance(5,7))
print(p.x,p.y)





