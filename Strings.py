x = "one two three four"
#Print the last 3 characters of x
def one():
	return x[-3:]

#Print the first 10 characters of x
def two():
	return x[:10]

#Print characetrs 4 through 10 of x
def three():
	return x[4:10]

#Find the length of x
def four():
	return len(x)

#Split x into its words
def five():
	return x.split(' ')

#Capitalize the first character of x
def six():
	return x.capitalize()


#Convert x into uppercase
def seven():
	return x.upper()


print(one())
print(two())
print(three())
print(four())
print(five())
print(six())
print(seven())
print("Strings")

