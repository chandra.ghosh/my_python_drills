#Save the first n natural numbers and their squares into a file in the csv format.
import csv
file_path=open("sqares.csv", "w")
def numbers_and_squares(n, file_path):
    a=1
    while (a<=n):
        file_path.write(str(a))
        file_path.write(',')
        file_path.write(str(a**2))
        if a!=n:
            file_path.write(',')
        a+=1
numbers_and_squares(4, file_path)