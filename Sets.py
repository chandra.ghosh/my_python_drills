#Define a function that does the equivalent of uniq <file_path> | sort, i.e., the function should
'''
1.Accept a file path as the argument
2.Read the contents of the file into a string
3.Split the string into lines
4.Remove duplicate lines
5.Sort the lines
6.Return the sorted lines
'''
def uniq(file_path):
    with open(file_path, 'r') as read_file:
        str=read_file.read()
    str2=str.split('\n')
    s2=set(str2)
    str2.sort()
    print(s2)
    return str2

print(uniq("Sets.txt"))
