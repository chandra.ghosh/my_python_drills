#To the n_digit_primes function, set the default value of argument n to 2
def n_digit_primes(num_of_digits=2):
	primes=[]
	for num in range(10**(num_of_digits-1), 10**(num_of_digits)):
		status=is_prime(num)
		if status:
			primes.append(num)
	return primes
def is_prime(num):
	for x in range(2, int(num/2+1)):
		if(num%x==0):
			return False
		else:
			continue
	return True

#Call the n_digit_primes with a keyword argument, n_digit_primes(n=1)
print(n_digit_primes(num_of_digits=3))

#Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.
def args_sum(*args):
    sum=0
    for x in args:
        sum+=x
    print(sum)
args_sum(2+3+15)

#Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute. If absolute is True, then args_sum must return
#the absolute value of the sum of *args. If absolute is not specified, then return the sum without performing any conversion.
