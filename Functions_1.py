#Write a function that accepts an integer n, n > 0, and returns a list of all n-digit prime numbers
def n_digit_primes(num_of_digits=2):
	primes=[]
	for num in range(10**(num_of_digits-1), 10**(num_of_digits)):
		status=is_prime(num)
		if status:
			primes.append(num)
	return primes

def is_prime(num):
	for x in range(2,int(num/2+1)):
		if(num%x==0):
			return False
		else:
			continue
	return True

print(n_digit_primes(2))