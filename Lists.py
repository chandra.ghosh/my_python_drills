x = [3, 1, 2, 4, 1, 2]
y = [5, 1, 2, 3]

#Find the sum of all the elements in x
def sumx():
	return sum(x)

#Find the length of x
def lenx():
	return len(x)

#Print the last three items of x
def last3():
	return x[-3:]

#Print the first three items of x
def first3():
	return x[:3]

#Sort x
def sortx():
	x.sort()
	return x

#Add another item, 10 to x
def append():
	x.append(10)
	return x

#Add another item 11 to x
def append2():
	x.append(11)
	return x

#Remove the last item from x
def delx():
	del x[-1]
	return x

#How many times does 1 occur in x?
def occu1():
	return x.count(1)

#Check whether the element 10 is present in x
def status(n):
	if n in x:
		return "true"
	else:
		return "false"


#Add all the elements of y to x
def extend():
	x.extend(y)
	return x

#Create a copy of x
def copy():
	n=x.copy()
	return n

#Can a list contain elements of different data-types?
def possibilityOfListsOfDiffDataType():
	listOfDiffDataType=[1,'x']
	return listOfDiffDataType



#Which data structure does Python use to implement a list?


print(sumx())
print(lenx())
print(last3())
print(first3())
print(sortx())
print(append())
print(append2())
print(delx())
print(occu1())
print(status(5))
print(extend())
print(copy())
print(possibilityOfListsOfDiffDataType())
print("Lists")
